package com.useradgents.httpswww.pmzambou

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.Html
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.useradgents.httpswww.pmzambou.Database.Database
import com.useradgents.httpswww.pmzambou.Database.MenuFoodItem
import kotlinx.android.synthetic.main.activity_menu_cart.*

var initialized: Boolean = false
var items = mutableListOf<MenuFoodItem>()
var cartIsSaved = false

class CaretActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var database: Database
    var cartAmount = 0
    lateinit var total: MenuItem
    lateinit var adapter: CartListAdapter
    lateinit var coordinatorLayout: CoordinatorLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        database = Database(this)
        if (items.size <= 0 && !initialized) initializedMutList()
        if (items.size <= 0) setContentView(R.layout.empty_cart)
        else {
            adapter = CartListAdapter(items, this)
            setContentView(R.layout.activity_menu_cart)
            val recyclerView: RecyclerView = findViewById(R.id.caret_recycler_view)
            recyclerView.layoutManager = LinearLayoutManager(this)
            recyclerView.adapter = adapter
            checkout.setOnClickListener(this)
            save_the_cart.setOnClickListener(this)
            coordinatorLayout = findViewById(R.id.coordinator_layout)
        }
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Panier"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)


    }

    override fun onStop() {
        if (!cartIsSaved) {
            val sharedPreferences = getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putBoolean("cartIsSaved", false)
            editor.apply()
            database.resetQuantities()
        }
        super.onStop()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_cart_activity, menu)
        total = menu!!.findItem(R.id.total)
        updateCartAmount()
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.deleteCart -> {
                if (items.size <= 0) {
                    Toast.makeText(this, "Votre panier est vide", Toast.LENGTH_LONG).show()
                } else {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Suppression du panier")
                    builder.setMessage("Etes-vous sûr de vouloir supprimer ce panier?")
                    builder.setPositiveButton("Supprimer") { _, _ -> deleteCart() }
                    builder.setNegativeButton("Annuler") { _, _ -> }
                    builder.show()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.plus -> {
                val cardView = (v.parent).parent as CardView
                val index = cardView.tag as Int
                MenuFoodListActivity.addToList(items[index])
                adapter.notifyDataSetChanged()
                updateCartAmount()
                MenuFoodListActivity.updateNumberOfElements()
            }
            R.id.minus -> {
                val cardView = (v.parent).parent as CardView
                val index = cardView.tag as Int
                updateCartMinus(items[index], index)
                cardView.callOnClick()
            }
            R.id.checkout -> {
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Commander")
                builder.setMessage("Votre commande a bien été enregistrée")
                builder.setPositiveButton("Ok") { _, _ -> }
                builder.show()
            }
            R.id.save_the_cart -> {
                saveTheCart()
                val sharedPreferences = getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putBoolean("cartIsSaved", true)
                editor.apply()
                cartIsSaved = true
                Snackbar.make(coordinatorLayout, Html.fromHtml("<font color=\"#ffffff\">Votre panier a bien été sauvegardé</font>"), Snackbar.LENGTH_LONG).show()
            }
        }
    }


    private fun initializedMutList() {
        database = Database(this)
        items = database.getAllItems()
        MenuFoodListActivity.selectItems()
        initialized = true
    }

    private fun updateCartAmount() {
        cartAmount = 0
        val iterator = items.listIterator()
        while (iterator.hasNext()) {
            val item = iterator.next()
            cartAmount += item.quantity * item.price
        }
        total.title = CatalogueItems.convert(cartAmount)
    }

    private fun deleteCart() {
        items.clear()
        adapter.notifyDataSetChanged()
        updateCartAmount()
        MenuFoodListActivity.updateNumberOfElements()
        save_the_cart.isEnabled = false
        checkout.isEnabled = false
    }

    private fun updateCartMinus(m: MenuFoodItem, i: Int) {
        when (m.quantity) {
            1 -> {
                items.removeAt(i)
                adapter.notifyDataSetChanged()
                updateCartAmount()
                MenuFoodListActivity.updateNumberOfElements()
                if (items.size <= 0) deleteCart()
            }
            else -> {
                m.quantity = m.quantity - 1
                items[i] = m
                adapter.notifyDataSetChanged()
                updateCartAmount()
                MenuFoodListActivity.updateNumberOfElements()
            }
        }
    }

    private fun saveTheCart() {
        for (i in 0 until items.size) {
            database.updateQuantity(items[i])
        }
    }
}

