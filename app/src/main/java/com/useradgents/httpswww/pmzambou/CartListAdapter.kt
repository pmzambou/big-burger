package com.useradgents.httpswww.pmzambou

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.useradgents.httpswww.pmzambou.Database.MenuFoodItem

class CartListAdapter(val caretList: MutableList<MenuFoodItem>, val itemClickListener: View.OnClickListener)
    : RecyclerView.Adapter<CartListAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val quantity: TextView = itemView.findViewById(R.id.quantity)
        val title: TextView = itemView.findViewById(R.id.title)
        val minus: ImageButton = itemView.findViewById(R.id.minus)
        val plus: ImageButton = itemView.findViewById(R.id.plus)
        val cardView: CardView = itemView.findViewById(R.id.card_view_cart)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val viewItem = inflater.inflate(R.layout.cart_item, parent, false)
        return ViewHolder(viewItem)
    }

    override fun onBindViewHolder(holder: CartListAdapter.ViewHolder, position: Int) {
        if (caretList.isNotEmpty()) {
            val menuFoodItem = caretList[position]
            holder.quantity.text = "${menuFoodItem.quantity}"
            holder.title.text = menuFoodItem.name
            holder.cardView.tag = position
            holder.cardView.setOnClickListener(itemClickListener)
            holder.minus.setOnClickListener(itemClickListener)
            holder.plus.setOnClickListener(itemClickListener)
            when (menuFoodItem.quantity) {
                1 -> holder.minus.setImageResource(R.drawable.ic_cancel_black_24dp)
                else ->holder.minus.setImageResource(R.drawable.ic_remove_circle_outline_black_24dp)
            }
        }
    }

    override fun getItemCount(): Int {
        return caretList.size
    }

}