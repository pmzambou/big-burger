package com.useradgents.httpswww.pmzambou

import android.os.Parcel
import android.os.Parcelable

class CatalogueItems(val ref: String, val title: String, val description: String, val thumbnail: String, val price: Int) : Parcelable {


    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ref)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeString(thumbnail)
        parcel.writeInt(price)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CatalogueItems> {
        override fun createFromParcel(parcel: Parcel): CatalogueItems {
            return CatalogueItems(parcel)
        }

        override fun newArray(size: Int): Array<CatalogueItems?> {
            return arrayOfNulls(size)
        }

        fun convert(price: Int): String {
            val amount: Int = price.rem(100)
            val euro: Int = (price - amount) / 100
            val cents: String
            if (amount <= 9) {
                cents = "0$amount"
            } else {
                cents = "$amount"
            }
            return "$euro,$cents€"
        }
    }
}