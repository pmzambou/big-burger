package com.useradgents.httpswww.pmzambou

import android.app.Activity
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

class CatalogueItemsAdapter(val activity: Activity, val catalogueItemsList: MutableList<CatalogueItems>, val itemClickListener: View.OnClickListener)
    : RecyclerView.Adapter<CatalogueItemsAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val icon: ImageView = itemView.findViewById(R.id.icon)
        val title: TextView = itemView.findViewById(R.id.title)
        val description: TextView = itemView.findViewById(R.id.description)
        val price: TextView = itemView.findViewById(R.id.price)
        val add: ImageButton = itemView.findViewById(R.id.add)
        val info: ImageButton = itemView.findViewById(R.id.info)

        val cardView: CardView = itemView.findViewById(R.id.card_view)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val viewItem = inflater.inflate(R.layout.menu_food_item, parent, false)
        return ViewHolder(viewItem)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val catalogueItem = catalogueItemsList[position]
        if (catalogueItem.ref=="7")holder.icon.setImageResource(R.drawable.sept)
        else {
            Glide.with(activity).load(catalogueItem.thumbnail).apply(RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)).into(holder.icon)
        }
        holder.title.text = catalogueItem.title
        holder.description.text = catalogueItem.description
        holder.price.text = CatalogueItems.convert(catalogueItem.price)
        holder.cardView.tag = position
        holder.cardView.setOnClickListener(itemClickListener)
        holder.add.setOnClickListener(itemClickListener)
        holder.info.setOnClickListener(itemClickListener)
    }

    override fun getItemCount(): Int {
        return catalogueItemsList.size
    }

}