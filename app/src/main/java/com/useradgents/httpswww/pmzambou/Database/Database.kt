package com.useradgents.httpswww.pmzambou.Database

import android.content.ContentValues
import android.content.Context
import android.database.DatabaseUtils
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class Database(context: Context) :
        SQLiteOpenHelper(context, "menu.db", null, 1) {
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL("CREATE TABLE items(id TEXT PRIMARY KEY, name TEXT, quantity INTEGER, price INTEGER)")
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
    }

    fun createMenuFoodItem(item: MenuFoodItem) {
        val values = ContentValues()
        values.put("id", item.id)
        values.put("name", item.name)
        values.put("quantity", item.quantity)
        values.put("price", item.price)
        writableDatabase.insert("items", null, values)
    }

    fun getAllItems(): MutableList<MenuFoodItem> {
        val items = mutableListOf<MenuFoodItem>()
        readableDatabase.rawQuery("SELECT*FROM items", null).use { cursor ->
            while (cursor.moveToNext()) {
                val item = MenuFoodItem(
                        cursor.getString(cursor.getColumnIndex("id")),
                        cursor.getString(cursor.getColumnIndex("name")),
                        cursor.getInt(cursor.getColumnIndex("quantity")),
                        cursor.getInt(cursor.getColumnIndex("price"))
                )
                items.add(item)
            }
        }
        return items
    }

    fun updateQuantity(m: MenuFoodItem) {
        val values = ContentValues()
        values.put("id", m.id)
        values.put("name", m.name)
        values.put("quantity", m.quantity)
        values.put("price", m.price)
        writableDatabase.update("items", values, "id=" + m.id, null)
    }

    fun resetQuantities() {
        val values = ContentValues()
        values.put("quantity", 0)
        writableDatabase.update("items", values, "quantity", null)
    }

    fun getItemsCount(): Int = DatabaseUtils.queryNumEntries(readableDatabase, "items", null).toInt()
}