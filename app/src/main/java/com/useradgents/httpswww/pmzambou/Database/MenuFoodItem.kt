package com.useradgents.httpswww.pmzambou.Database

data class MenuFoodItem(val id: String, val name: String, var quantity: Int, val price: Int)