package com.useradgents.httpswww.pmzambou

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.Html
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.useradgents.httpswww.pmzambou.Database.Database
import com.useradgents.httpswww.pmzambou.Database.MenuFoodItem
import kotlinx.android.synthetic.main.activity_menu_food_list.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException

private val catalogue = mutableListOf<CatalogueItems>()

class MenuFoodListActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var mDrawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_food_list)

        mDrawerLayout = findViewById(R.id.drawer_layout)
        val navigationView: NavigationView = findViewById(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener { menuItem ->
            menuItem.isChecked = true
            mDrawerLayout.closeDrawers()
            Toast.makeText(this, "Vous avez appuyé sur ${menuItem.title}", Toast.LENGTH_LONG).show()
            true
        }

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Big Burger France"
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.baseline_menu_black_18dp)
        }

        val database = Database(this)
        if (database.getItemsCount() == 0) populateDataBase(database)

        if (catalogue.size <= 0) loadCatalogue()
        val adapter = CatalogueItemsAdapter(this, catalogue, this)
        val recyclerView: RecyclerView = findViewById(R.id.menu_food_recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        val sharedPreference = getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        val hasBeenSaved = sharedPreference.getBoolean("cartIsSaved", false)
        if (hasBeenSaved) {
            items = database.getAllItems()
            selectItems()
        }
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        isNetworkAvailable(cm)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_menu_food_list, menu)
        nbOfElements = menu.findItem(R.id.nbOfElements)
        updateNumberOfElements()
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_show_caret -> {
                val intent = Intent(this, CaretActivity::class.java)
                startActivity(intent)
            }
            android.R.id.home -> {
                mDrawerLayout.openDrawer(GravityCompat.START)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(view: View) {
        if (view.tag != null) {
            clickOnCardView(view)
        } else {
            clickOnButton(view)
        }
    }

    private fun clickOnCardView(v: View) {
        val index = v.tag as Int
        val item = catalogue[index]
        val intent = Intent(this, ShowMenuDetailsActivity::class.java)
        intent.putExtra("selectedItem", item)
        startActivity(intent)
    }

    private fun clickOnButton(v: View) {
        when (v.id) {
            R.id.add -> {
                val cardView = (v.parent).parent.parent.parent as CardView
                val parentIndex = cardView.tag as Int
                val food = catalogue[parentIndex]
                val menuFoodItem = MenuFoodItem(food.ref, food.title, 1, food.price)
                addToList(menuFoodItem)
                MenuFoodListActivity.updateNumberOfElements()
                Toast.makeText(this, "1 ${food.title} a bien été ajouté au panier", Toast.LENGTH_LONG).show()
            }

            R.id.info -> {
                val cardView = (v.parent).parent.parent as CardView
                clickOnCardView(cardView)
            }
        }
    }

    private fun populateDataBase(db: Database) {
        for (item: CatalogueItems in catalogue) {
            db.createMenuFoodItem(MenuFoodItem(item.ref, item.title, 0, item.price))
        }
    }


    companion object {
        lateinit var nbOfElements: MenuItem
        fun addToList(m: MenuFoodItem) {
            var added = false
            val iterator = items.listIterator()
            while (iterator.hasNext()) {
                val item = iterator.next()
                val newItem = MenuFoodItem(item.id, item.name, item.quantity + 1, item.price)
                if (item.id == m.id) {
                    iterator.set(newItem)
                    added = true
                }
            }
            if (!added) items.add(m)
        }

        fun selectItems() {
            val iterator = items.listIterator()
            while (iterator.hasNext()) {
                val item = iterator.next()
                if (item.quantity == 0)
                    iterator.remove()
            }
        }

        fun updateNumberOfElements() {
            var total = 0
            val iterator = items.listIterator()
            while (iterator.hasNext()) {
                val item = iterator.next()
                total += item.quantity
            }
            nbOfElements.title = total.toString()
        }
    }

    private fun loadCatalogue() {
        try {
            val fileText = applicationContext.assets.open("catalogue.json").bufferedReader(Charsets.UTF_8).use { it.readText() }
            val a = JSONArray(fileText)
            for (i in 0 until a.length()) {
                val j: JSONObject = a[i] as JSONObject
                val ref = j.getString("ref")
                val title = j.getString("title")
                val description = j.getString("description")
                val thumbnail = j.getString("thumbnail")
                val price = j.getInt("price")
                val item = CatalogueItems(ref, title, description, thumbnail, price)
                catalogue.add(item)
            }
        } catch (e: IOException) {
        }
    }


    private fun isNetworkAvailable(cm: ConnectivityManager) {
        val networkInfo = cm.activeNetworkInfo
        if (networkInfo == null || !networkInfo.isConnected) {
            Snackbar.make(coordinator_layout, Html.fromHtml("<font color=\"#ffffff\">Erreur lors du chargement. Vérifiez votre connexion réseau</font>"), Snackbar.LENGTH_LONG).show()
        }
    }
}

