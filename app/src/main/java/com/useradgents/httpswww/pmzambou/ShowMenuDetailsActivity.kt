package com.useradgents.httpswww.pmzambou

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.useradgents.httpswww.pmzambou.Database.MenuFoodItem
import kotlinx.android.synthetic.main.activity_show_menu_details.*

class ShowMenuDetailsActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var menuFoodItem: MenuFoodItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_menu_details)
        val icon: ImageView = findViewById(R.id.icon)

        val extras = intent.extras as Bundle
        val catalogItem = extras.getParcelable<CatalogueItems>("selectedItem")

        if (catalogItem!!.ref=="7")icon.setImageResource(R.drawable.sept)
        else {
            Glide.with(this).load(catalogItem.thumbnail).apply(RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)).into(icon)
        }

        price.text = CatalogueItems.convert(catalogItem.price)
        description.text = catalogItem.description


        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = catalogItem.title
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        add_to_caret.setOnClickListener(this)
        menuFoodItem = MenuFoodItem((catalogItem.ref), catalogItem.title, 1, catalogItem.price)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_show_details_activity, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_show_caret -> {
                startCaretActivity()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.add_to_caret -> {
                MenuFoodListActivity.addToList(menuFoodItem)
                startCaretActivity()
            }
        }
    }

    fun startCaretActivity() {
        val intent = Intent(this, CaretActivity::class.java)
        startActivity(intent)
    }
}
